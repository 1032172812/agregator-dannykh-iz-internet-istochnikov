package com.qrent.services;

import com.qrent.config.JpaAuditingConfig;
import com.qrent.config.SecurityConfig;
import com.qrent.dao.entities.ERole;
import com.qrent.dao.entities.Role;
import com.qrent.dto.request.SignupRequest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes={ SecurityConfig.class, JpaAuditingConfig.class })
public class UserServiceTest {
    @Autowired
    private UserServiceImpl userService;

    @Before
    public void before() {
    }

    @Test
    public void register() {//TODO: не работает
        Set<String> roles = new HashSet<>();
        roles.add(ERole.ROLE_USER.name());
        SignupRequest signupRequest = new SignupRequest("testUser", "test@mail.ru", roles, "qweqwe");

        ResponseEntity<?> register = userService.register(signupRequest);
        Assert.assertEquals(register, ResponseEntity.ok("User registered successfully!"));
    }
}
