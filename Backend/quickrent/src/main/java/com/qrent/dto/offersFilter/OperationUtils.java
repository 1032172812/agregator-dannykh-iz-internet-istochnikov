package com.qrent.dto.offersFilter;

import java.util.List;

public class OperationUtils {
    public static Operation compare(String compareSign) {
        Operation operation = new Operation() {
            @Override
            public String apply(String x, Object y) {
                String value = "";
                if (y instanceof String) {
                    value = "'" + y + "'";
                }
                else {
                    value = y.toString();
                }

                return "(" + x + " " + compareSign + " " + value + ")";
            }
        };

        return operation;
    }

    public static Operation in() {
        Operation operation = new Operation<List<Object>>() {
            @Override
            public String apply(String x, List<Object> y) {
                String res = "";
                res += "(" + x + " in ";

                res += "(" + y.stream().map(item -> (item instanceof String) ? "'" + item + "'" : item.toString())
                        .reduce((a, b) -> a + ", " + b).orElse("") + "))";

                return res;
            }
        };

        return operation;
    }

    public static Operation like() {
        Operation operation = new Operation<List<String>>() {
            @Override
            public String apply(String x, List<String> y) {
                String res = "";

                res += y.stream().map(item -> ("(" + x + " LIKE CONCAT('%', '" + item + "','%'))"))
                        .reduce((a, b) -> a + " or " + b).orElse("");

                return res;
            }
        };

        return operation;
    }

    public static Operation notLike() {
        Operation operation = new Operation<List<String>>() {
            @Override
            public String apply(String x, List<String> y) {
                String res = "";

                res += y.stream().map(item -> ("(" + x + " NOT LIKE CONCAT('%', '" + item + "','%'))"))
                        .reduce((a, b) -> a + " and " + b).orElse("");

                return res;
            }
        };

        return operation;
    }

    public static Operation blankOperation() {
        Operation operation = new Operation<String>() {
            @Override
            public String apply(String x, String y) {
                return "(" + x + " " + y + ")";
            }
        };

        return operation;
    }
}
