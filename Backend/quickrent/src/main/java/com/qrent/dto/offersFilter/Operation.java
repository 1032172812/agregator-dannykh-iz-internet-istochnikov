package com.qrent.dto.offersFilter;

@FunctionalInterface
public interface Operation <T> {
    String apply(String x, T y);
}
