package com.qrent.dto.offersFilter;

import com.qrent.parsers.RentalTime;
import com.qrent.parsers.RoomsType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class OffersFilter implements HqlSubquery {
    private List<OffersCondition> conditions = new ArrayList<>();
    private static String TABLE_NAME = "Offer";

    public static String getTableName() {
        return TABLE_NAME;
    }

    public OffersFilter(Map<String, Object> requestData) {
        if (requestData == null) {
            return;
        }

        if (requestData.get("price") != null) {
            String from = (String) ((Map) requestData.get("price")).get("from");
            String to = (String) ((Map) requestData.get("price")).get("to");

            if (from != null && from != "") {
                conditions.add(new OffersCondition("price",
                        OperationUtils.compare(OperationType.MORE_EQ.getSign()), Double.parseDouble(from)));
            }

            if (to != null && to != "") {
                conditions.add(new OffersCondition("price",
                        OperationUtils.compare(OperationType.LESS_EQ.getSign()), Double.parseDouble(to)));
            }
        }

        if (requestData.get("rooms") != null) {
            List<Integer> types = ((List<String>) requestData.get("rooms")).stream()
                    .map(item -> RoomsType.valueOf(item).getNumber())
                    .collect(Collectors.toList());

            conditions.add(new OffersCondition("rooms", OperationUtils.in(), types));
        }

        if (requestData.get("area") != null) {
            String from = (String) ((Map) requestData.get("area")).get("from");
            String to = (String) ((Map) requestData.get("area")).get("to");

            if (from != null && from != "") {
                conditions.add(new OffersCondition("area",
                        OperationUtils.compare(OperationType.MORE_EQ.getSign()), Double.parseDouble(from)));
            }

            if (to != null && to != "") {
                conditions.add(new OffersCondition("area",
                        OperationUtils.compare(OperationType.LESS_EQ.getSign()), Double.parseDouble(to)));
            }
        }

        if (requestData.get("rent-type") != null) {
            String type = RentalTime.valueOf(((String) requestData.get("rent-type"))).getTime();

            conditions.add(new OffersCondition("rentalTime",
                    OperationUtils.in(), Arrays.asList(type)));
        }

        if (requestData.get("street") != null && requestData.get("street") != "") {
            String street = (String) requestData.get("street");

            conditions.add(new OffersCondition("address",
                    OperationUtils.like(), street));
        }

        if (requestData.get("add-keyword") != null && requestData.get("add-keyword") != "") {
            String wordsString = (String) requestData.get("add-keyword");
            List<String> words = Arrays.asList(wordsString.split(", "));

            if (!words.isEmpty()) {
                conditions.add(new OffersCondition("description",
                        OperationUtils.like(), words));
            }
        }

        if (requestData.get("remove-keyword") != null && requestData.get("remove-keyword") != "") {
            String wordsString = (String) requestData.get("remove-keyword");
            List<String> words = Arrays.asList(wordsString.split(", "));

            if (!words.isEmpty()) {
                conditions.add(new OffersCondition("description",
                        OperationUtils.notLike(), words));
            }
        }

        if (requestData.get("floor") != null) {
            String from = (String) ((Map) requestData.get("floor")).get("from");
            String to = (String) ((Map) requestData.get("floor")).get("to");

            if (from != null && from != "") {
                conditions.add(new OffersCondition("floor",
                        OperationUtils.compare(OperationType.MORE_EQ.getSign()), Integer.parseInt(from)));
            }

            if (to != null && to != "") {
                conditions.add(new OffersCondition("floor",
                        OperationUtils.compare(OperationType.LESS_EQ.getSign()), Integer.parseInt(to)));
            }
        }

        if (requestData.get("maxFloor") != null) {
            String from = (String) ((Map) requestData.get("maxFloor")).get("from");
            String to = (String) ((Map) requestData.get("maxFloor")).get("to");

            if (from != null && from != "") {
                conditions.add(new OffersCondition("maxFloor",
                        OperationUtils.compare(OperationType.MORE_EQ.getSign()), Integer.parseInt(from)));
            }

            if (to != null && to != "") {
                conditions.add(new OffersCondition("maxFloor",
                        OperationUtils.compare(OperationType.LESS_EQ.getSign()), Integer.parseInt(to)));
            }
        }
    }

    public void addSubquery(OffersCondition condition) {
        if (condition != null)
            conditions.add(condition);
    }

    public String toHql() {
        String where = "";
        String conditionsString = conditions.stream()
                .map(item -> item.toHql())
                .reduce((x, y) -> x + " and " + y)
                .orElse(null);

        if (conditionsString != null) {
            where = " where " + conditionsString;
        }

        return "from " + TABLE_NAME + where;
    }
}
