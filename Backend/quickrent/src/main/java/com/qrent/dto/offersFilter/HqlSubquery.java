package com.qrent.dto.offersFilter;

public interface HqlSubquery {
    /**
     * Перевод подзапроса в hql
     *
     * @return hql подзапрос
     */
    String toHql();
}
