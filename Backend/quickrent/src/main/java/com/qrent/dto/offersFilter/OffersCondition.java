package com.qrent.dto.offersFilter;

public class OffersCondition<T> implements HqlSubquery {
    String name;
    Operation operation;
    T value;

    public OffersCondition(String name, Operation<T> operation, T value) {
        this.name = name;
        this.operation = operation;
        this.value = value;
    }

    @Override
    public String toHql() {
        return operation.apply(name, value);
    }
}
