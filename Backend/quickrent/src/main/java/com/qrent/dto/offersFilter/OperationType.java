package com.qrent.dto.offersFilter;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OperationType {
    EQUALS("="),
    MORE(">"),
    LESS("<"),
    MORE_EQ(">="),
    LESS_EQ("<=");
    private String sign;
}
