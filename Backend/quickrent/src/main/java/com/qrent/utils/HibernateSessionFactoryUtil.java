package com.qrent.utils;

import com.qrent.dao.entities.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

@Service
public class HibernateSessionFactoryUtil {
    private static SessionFactory sessionFactory;

    private HibernateSessionFactoryUtil() {
        Configuration configuration = new Configuration().configure();
        configuration.addAnnotatedClass(Offer.class);
        configuration.addAnnotatedClass(Location.class);
        configuration.addAnnotatedClass(User.class);
        configuration.addAnnotatedClass(Role.class);

        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        sessionFactory = configuration.buildSessionFactory(builder.build());
    }

    @Bean(name = "entityManagerFactory")
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
