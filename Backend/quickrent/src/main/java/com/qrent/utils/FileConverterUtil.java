package com.qrent.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

public class FileConverterUtil {
    public static byte[] multipartFileToBytes(MultipartFile file) {
        if (file == null)
            return null;

        InputStream initialStream = null;
        try {
            initialStream = file.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte[] buffer = new byte[0];
        try {
            buffer = new byte[initialStream.available()];
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            initialStream.read(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return buffer;
    }
}
