package com.qrent.services;

import com.qrent.controllers.MarkDto;
import com.qrent.dao.OfferDao;
import com.qrent.dao.entities.Location;
import com.qrent.dao.entities.Offer;
import com.qrent.dto.offersFilter.OffersFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class OfferService {
    @Autowired
    private OfferDao offerDao;

    public List<Offer> getOffers(Map<String, Object> requestData, int page, int cnt) {
        OffersFilter filter = new OffersFilter(requestData);

        return offerDao.getByFilter(filter).stream().skip(page * cnt).limit(cnt).collect(Collectors.toList());
    }

    public Integer count(Map<String, Object> requestData) {
        if (requestData.get("filters") != null) {
            requestData = (Map<String, Object>) requestData.get("filters");
        }
        return offerDao.getByFilter(new OffersFilter(requestData)).size();
    }

    public Offer getById(long id) {
        return offerDao.getById(id);
    }

    public List<MarkDto> getMarks() {
        final double minLat = 57.531964;
        final double maxLat = 57.674284;
        final double minLon = 39.630681;
        final double maxLon = 40.011674;

        return offerDao.getMarks().stream()
                .filter(item -> (item.getLocation().getLongitude() > minLon
                        && item.getLocation().getLongitude() < maxLon
                        && item.getLocation().getLatitude() > minLat
                        && item.getLocation().getLatitude() < maxLat))
                .sorted(new MarkComparator()).collect(Collectors.toList());
    }

    class MarkComparator implements Comparator<MarkDto> {
        public int compare(MarkDto a, MarkDto b) {
            Location la = a.getLocation();
            Location lb = b.getLocation();

            if (lb.getLatitude() > la.getLatitude()) {
                return 1;
            } else if (lb.getLatitude() < la.getLatitude()) {
                return -1;
            } else {
                if (lb.getLongitude() > la.getLongitude()) {
                    return 1;
                } else if (lb.getLongitude() < la.getLongitude()) {
                    return -1;
                }
            }
            return 0;
        }
    }
}
