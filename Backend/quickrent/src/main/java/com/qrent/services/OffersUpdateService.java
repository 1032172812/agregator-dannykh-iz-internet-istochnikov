package com.qrent.services;

import com.qrent.dao.OfferDao;
import com.qrent.dao.entities.Offer;
import com.qrent.parsers.OffersParser;
import com.qrent.parsers.domofond.DomofondParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@EnableScheduling
public class OffersUpdateService {
    @Autowired
    private OfferDao offerDao;

    private List<OffersParser> parsers;

    @Autowired
    public void setParsers(DomofondParser domofondParser) {
        parsers = new ArrayList<>();
        parsers.add(domofondParser);
    }

    @Scheduled(fixedDelay = 3600 * 1000, initialDelay = 3600 * 1000)
    public void removeInvalidOffers() {
        offerDao.removeInvalid();
    }

    @Scheduled(fixedDelay = 3600 * 1000, initialDelay = 0)
    public void loadNewOffers() {
//        List<Offer> offers = new ArrayList<>();

//        parsers.forEach(parser -> offers.addAll(parser.parseAll()));

//        offers.forEach(offer -> offerDao.createOrUpdate(offer));
    }
}
