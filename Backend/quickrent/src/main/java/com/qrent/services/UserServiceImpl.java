package com.qrent.services;

import com.qrent.dao.RoleRepository;
import com.qrent.dao.UserRepository;
import com.qrent.dao.entities.ERole;
import com.qrent.dao.entities.Role;
import com.qrent.dao.entities.User;
import com.qrent.dto.request.LoginRequest;
import com.qrent.dto.request.SignupRequest;
import com.qrent.jwt.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

import static com.qrent.utils.FileConverterUtil.multipartFileToBytes;

@Service
public class UserServiceImpl {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Transactional
    public Object auth(LoginRequest loginRequest) {
        try {
            String username = loginRequest.getUsername();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, loginRequest.getPassword()));
            User user = findByUsername(username);

            if (user == null) {
                throw new UsernameNotFoundException("User with username: " + username + " not found");
            }

            return getAuthTokens(user);
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username or password");
        }
    }

    public boolean isTokenValid(String token) {
        return jwtTokenProvider.validateToken(token);
    }

    private Map<String, String> getAuthTokens(User user) {
        String token = user.getToken();
        String accessToken;
        String refreshToken;

        if (token != null && jwtTokenProvider.validateToken(token)) {
            refreshToken = user.getToken();
            accessToken = jwtTokenProvider.createAccessToken(user.getUsername(), new ArrayList<>(user.getRoles()), jwtTokenProvider.parseDate(token));
        }
        else {
            String uuid = UUID.randomUUID().toString();
            Date now = new Date();

            accessToken = jwtTokenProvider.createAccessToken(user.getUsername(), new ArrayList<>(user.getRoles()), new Date());
            refreshToken = jwtTokenProvider.createRefreshToken(user.getUsername(), uuid, now);

            user.setToken(refreshToken);
        }

        Map<String, String> response = new HashMap<>();
        response.put("accessToken", accessToken);
        response.put("refreshToken", refreshToken);

        return response;
    }

    @Transactional
    public Map<String, String> updateTokens(String oldToken) {
        if (!jwtTokenProvider.validateToken(oldToken)) {
            return null;
        }

        String username = jwtTokenProvider.getUsername(oldToken);
        if (username == null) {
            return null;
        }

        User user = userRepository.findByUsername(username).orElse(null);
        if (user != null) {
            String uuid = UUID.randomUUID().toString();
            Date now = new Date();

            String accessToken = jwtTokenProvider.createAccessToken(user.getUsername(), new ArrayList<>(user.getRoles()), new Date());
            String refreshToken = jwtTokenProvider.createRefreshToken(user.getUsername(), uuid, now);

            user.setToken(refreshToken);

            Map<String, String> response = new HashMap<>();
            response.put("accessToken", accessToken);
            response.put("refreshToken", refreshToken);

            return response;
        }

        return null;
    }

    @Transactional
    public ResponseEntity<?> register(SignupRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body("Error: Username is already taken!");
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body("Error: Email is already in use!");
        }

        // Create new user's account
        User user = new User(signUpRequest.getUsername(),
                signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()));

        Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));

        Set<Role> roles = new HashSet<>();
        roles.add(userRole);

        user.setRoles(roles);
        userRepository.save(user);

        return ResponseEntity.ok("User registered successfully!");
    }

    public String getCurrentUsername() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getName();
    }

    public User getCurrentUser() {
        return findByUsername(getCurrentUsername());
    }

    public User getUserById(long id) {
        return userRepository.getOne(id);
    }

    public User findByUsername(String name) {
        return userRepository.findByUsername(name).orElse(null);
    }

    @Transactional
    public void updateAvatar(MultipartFile image) {
        User user = getCurrentUser();
        user.setAvatar(multipartFileToBytes(image));
        userRepository.saveAndFlush(user);
    }

    @Transactional
    public void changePassword(String oldPass, String newPass) {
        User user = getCurrentUser();

        if (user.getPassword().equals(encoder.encode(oldPass))) {
            user.setPassword(encoder.encode(newPass));
            userRepository.save(user);
        }
    }
}
