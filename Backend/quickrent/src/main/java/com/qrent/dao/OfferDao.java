package com.qrent.dao;

import com.qrent.controllers.MarkDto;
import com.qrent.dao.entities.Offer;
import com.qrent.dto.offersFilter.OffersFilter;

import java.util.List;

public interface OfferDao {
    void create(Offer offer);

    void create(List<Offer> offers);

    void delete(Offer offer);

    List<Offer> getByFilter(OffersFilter filter);

    void removeInvalid();

    boolean isExist(Offer offer);

    void update(Offer offer);

    void createOrUpdate(Offer offer);

    Offer getById(long id);

    List<MarkDto> getMarks();
}
