package com.qrent.dao.entities;

import com.qrent.parsers.RentalTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "offers")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Offer extends Auditable<Date> {
    private static final int MAX_TEXT_LENGTH = 3000;

    @Id
    @GenericGenerator(
            name = "offer_sequence",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = "sequence_name", value = "OFFER_SEQUENCE"),
                    @Parameter(name = "initial_value", value = "1"),
                    @Parameter(name = "increment_size", value = "1")
            }
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "offer_sequence")
    private long id;

    @Column(name = "title")
    private String title = "Нет заголовка";

    @Column(name = "description", length = MAX_TEXT_LENGTH)
    private String description = "Нет описания";

    @Column(name = "price")
    private double price = 0;

    @Column(name = "deposit")
    private double deposit = 0;

    @Column(name = "district")
    private String district = "Район не указан";

    @Column(name = "address")
    private String address = "Адрес не указан";

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name="imgUrl", joinColumns=@JoinColumn(name="offer_id"))
    @Column(name="imgUrls")
    private List<String> imgUrls;

    @Column(name = "floor")
    private Integer floor = -1;

    @Column(name = "maxFloor")
    private Integer maxFloor = -1;

    @Column(name = "area")
    private double area = 0;

    @Column(name = "rooms")
    private Integer rooms;

    @Column(name = "pageUrl")
    private String pageUrl;

    @Column(name = "rentalTime")
    private String rentalTime;

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private Location location;

    @Column(name = "publishDate")
    private Calendar publishDate;

    private Offer(Builder builder) {
        this.title = builder.title;
        this.description = builder.description;
        this.price = builder.price;
        this.deposit = builder.deposit;
        this.district = builder.district;
        this.address = builder.address;
        this.imgUrls = builder.imgUrls;
        this.floor = builder.floor;
        this.maxFloor = builder.maxFloor;
        this.area = builder.area;
        this.rooms = builder.rooms;
        this.pageUrl = builder.pageUrl;
        this.location = builder.location;
        this.rentalTime = builder.rentalTime;
        this.publishDate = builder.publishDate;
    }

    public static class Builder {
        private String title = "Нет заголовка";
        private String description = "Нет описания";
        private double price = 0; //TODO: need to use Double
        private double deposit = 0; //TODO: need to use Double
        private String district = "Район не указан";
        private String address = "Адрес не указан";
        private List<String> imgUrls;
        private Integer floor = -1;
        private Integer maxFloor = -1;
        private double area = 0;//TODO: need to use Double
        private Integer rooms;
        private String pageUrl;
        private String rentalTime = "";
        private Location location;
        private Calendar publishDate;

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder description(String description) {
            this.description = (description.length() < MAX_TEXT_LENGTH - 1)
                    ? description
                    : description.substring(0, MAX_TEXT_LENGTH - 4) + "...";

            return this;
        }

        public Builder price(double price) {
            this.price = price;
            return this;
        }

        public Builder deposit(double deposit) {
            this.deposit = deposit;
            return this;
        }

        public Builder district(String district) {
            this.district = district;
            return this;
        }

        public Builder address(String address) {
            this.address = address;
            return this;
        }

        public Builder imgUrls(List<String> imgUrls) {
            this.imgUrls = imgUrls;
            return this;
        }

        public Builder floor(Integer floor) {
            this.floor = floor;
            return this;
        }

        public Builder maxFloor(Integer floor) {
            this.maxFloor = floor;
            return this;
        }

        public Builder area(double area) {
            this.area = area;
            return this;
        }

        public Builder rooms(Integer rooms) {
            this.rooms = rooms;
            return this;
        }

        public Builder pageUrl(String pageUrl) {
            this.pageUrl = pageUrl;
            return this;
        }

        public Builder rentalTime(RentalTime rentalTime) {
            this.rentalTime = (rentalTime == null) ? "" : rentalTime.getName();

            return this;
        }

        public Builder location(Location location) {
            this.location = location;
            return this;
        }

        public Builder publishDate(Calendar publishDate) {
            this.publishDate = publishDate;
            return this;
        }

        public Offer build() {
            return new Offer(this);
        }
    }
}
