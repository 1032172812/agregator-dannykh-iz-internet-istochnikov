package com.qrent.dao.hibernate;

import com.qrent.controllers.MarkDto;
import com.qrent.dao.OfferDao;
import com.qrent.dao.entities.Location;
import com.qrent.dao.entities.Offer;
import com.qrent.dto.offersFilter.OffersFilter;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class OfferDaoHibernate implements OfferDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void create(Offer offer) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(offer);

        if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
            transaction.commit();
        }

        session.close();
    }

    @Override
    public void create(List<Offer> offers) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        offers.forEach(session::save);

        if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
            transaction.commit();
        }

        session.close();
    }

    @Override
    public void delete(Offer offer) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(offer);
        transaction.commit();
        session.close();
    }

    @Override
    public List<Offer> getByFilter(OffersFilter filter) {
        Session session = sessionFactory.openSession();

        Query query = session.createQuery(filter.toHql());
        List<Offer> offers = query.list();
        session.close();

        return offers;
    }

    @Override
    public void removeInvalid() {
        //TODO
    }

    private List<Offer> getOffersLike(Session session, Offer offer) {
        //TODO: FIX

        List<Offer> offers = new ArrayList<>();
        Query query = null;
        try {
//            query = session.createQuery("from Offer where imgUrls = :imgUrls");
//            query.setParameter("imgUrls",
//                    (offer.getImgUrls() != null && offer.getImgUrls().get(0) != null)?offer.getImgUrl():"", StringType.INSTANCE);
//            offers = query.list();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return offers;
    }

    @Override
    public boolean isExist(Offer offer) {
        Session session = sessionFactory.openSession();

        List<Offer> offers = getOffersLike(session, offer);

        session.close();
        return !offers.isEmpty();
    }

    @Override
    public void update(Offer offer) {

    }

    @Override
    public void createOrUpdate(Offer offer) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        List<Offer> offers = getOffersLike(session, offer);

        if (offers.isEmpty()) {
            session.save(offer);
        } else {
            session.update(offer);
        }

        transaction.commit();

        session.close();
    }

    @Override
    public Offer getById(long id) {
        Session session = sessionFactory.openSession();

        Query query = session.createQuery("from Offer where id = :id");
        query.setParameter("id", id);
        Offer offer = (Offer)query.uniqueResult();
        session.close();

        return offer;
    }

    @Override
    public List<MarkDto> getMarks() {
        Session session = sessionFactory.openSession();

        Query query = session.createQuery("from Offer");
        List<Offer> offers = query.list();
        List<MarkDto> marks = new ArrayList<>();
        offers.forEach(offer -> marks.add(
                new MarkDto(
                        offer.getLocation(),
                        (offer.getImgUrls() != null)?offer.getImgUrls().get(0):null,
                        offer.getTitle())
        ));
        session.close();

        return marks;
    }
}
