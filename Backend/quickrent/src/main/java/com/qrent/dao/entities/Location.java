package com.qrent.dao.entities;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;

@Entity
@Table(name = "locations")
@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class Location {
    @Id
    @GenericGenerator(
            name = "location_sequence",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = "sequence_name", value = "LOCATION_SEQUENCE"),
                    @Parameter(name = "initial_value", value = "1"),
                    @Parameter(name = "increment_size", value = "1")
            }
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "location_sequence")
    private long id;

    @Column(name = "latitude")
    @NonNull
    private double latitude;

    @Column(name = "longitude")
    @NonNull
    private double longitude;
}
