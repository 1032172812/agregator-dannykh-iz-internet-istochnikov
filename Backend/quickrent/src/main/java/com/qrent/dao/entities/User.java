package com.qrent.dao.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "users",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "username"),
                @UniqueConstraint(columnNames = "email")
        })

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class User extends Auditable<Date> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 20)
    @NonNull
    @Column(name = "username")
    private String username;

    @NotBlank
    @Size(max = 50)
    @Email
    @NonNull
    @Column(name = "email")
    private String email;

    @NotBlank
    @JsonIgnore
    @Size(max = 120)
    @NonNull
    @Column(name = "password")
    private String password;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    @JsonIgnore
    @Size(max = 800)
    @Column(name = "token")
    private String token;

    @Lob
    @Column(name = "avatar")
    private byte[] avatar;

    @Column(name = "allNotification")
    private boolean allNotification;

    @Column(name = "importantNotification")
    private boolean importantNotification;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status = Status.NOT_ACTIVE;
}
