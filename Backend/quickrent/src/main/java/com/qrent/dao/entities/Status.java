package com.qrent.dao.entities;

public enum Status {
    ACTIVE, NOT_ACTIVE, DELETED
}