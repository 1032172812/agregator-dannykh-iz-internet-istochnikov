package com.qrent.jwt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtTokenFilter extends OncePerRequestFilter {
    private static final Logger logger = LoggerFactory.getLogger(JwtTokenFilter.class);

    private JwtTokenProvider jwtTokenProvider;
    private HandlerExceptionResolver resolver;

    public JwtTokenFilter(JwtTokenProvider jwtTokenProvider, HandlerExceptionResolver resolver) {
        this.jwtTokenProvider = jwtTokenProvider;
        this.resolver = resolver;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException, NullPointerException {

        if (!(request.getRequestURL().toString().contains("api/all")
                || request.getRequestURL().toString().contains("api/auth/signup")
                || request.getRequestURL().toString().contains("api/auth/signin")
                || request.getRequestURL().toString().contains("api/auth/update-tokens"))) {
            try {
                String jwt = jwtTokenProvider.parseJwt(request);
                if (jwt != null && jwtTokenProvider.validateToken(jwt)) {
                    UsernamePasswordAuthenticationToken auth = jwtTokenProvider.getAuthentication(jwt);

                    if (auth != null) {
                        auth.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                        SecurityContextHolder.getContext().setAuthentication(auth);
                    }
                } else {
                    resolver.resolveException(request, response, null, new InvalidTokenException());
                }
            } catch (Exception e) {
                logger.error("Cannot set user authentication: {}", e);
                resolver.resolveException(request, response, null, new InvalidTokenException()); //TODO: FIX
            }
        }

        filterChain.doFilter(request, response);
    }
}
