package com.qrent.jwt.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;

@RequiredArgsConstructor
@Getter
public class JwtUser implements UserDetails {
    private static final long serialVersionUID = 1L;

    @NonNull
    private final Long id;

    @NonNull
    private final String username;

    @NonNull
    private final String password;

    @NonNull
    private final String email;

    @NonNull
    private final boolean enabled;

    @NonNull
    private final Date lastPasswordResetDate;

    @NonNull
    private final Collection<? extends GrantedAuthority> authorities;

    @Setter
    private boolean allNotification;

    @Setter
    private boolean importantNotification;

    @JsonIgnore
    public Long getId() {
        return id;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return password;
    }

}
