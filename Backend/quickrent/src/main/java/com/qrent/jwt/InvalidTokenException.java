package com.qrent.jwt;

import org.springframework.security.access.AccessDeniedException;

public class InvalidTokenException extends AccessDeniedException {
    public InvalidTokenException() {
        super("Invalid token");
    }
}
