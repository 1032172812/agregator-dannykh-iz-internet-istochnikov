package com.qrent.controllers;

import com.qrent.dao.entities.Location;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MarkDto {
    private Location location;
    private String imgUrl;
    private String title;
}
