package com.qrent.controllers;

import com.qrent.dao.entities.User;
import com.qrent.services.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UserServiceImpl userService;

    @PostMapping("/update-avatar")
    public ResponseEntity<String> update(@RequestParam(value = "image") MultipartFile image) {
        userService.updateAvatar(image);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/avatar", produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity<byte[]> getAvatarImage() {
        HttpHeaders headers = new HttpHeaders();
        byte[] media = null;
        try {
            media = userService.getCurrentUser().getAvatar();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        headers.setCacheControl(CacheControl.noCache().getHeaderValue());

        ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(media, headers, HttpStatus.OK);

        return responseEntity;
    }

    @GetMapping(value = "/check-token")
    public ResponseEntity<?> isTokenCorrect(@RequestParam(value = "token", required = true) String token) {
        ResponseEntity<?> result = null;
        if (userService.isTokenValid(token)) {
            result = ResponseEntity.ok("Token is valid!");
        } else {
            result = ResponseEntity
                    .badRequest()
                    .body("Error: Invalid token!");
        }

        return result;
    }

    @GetMapping(value = "/get-data")
    public User getData() {
        return userService.getCurrentUser();
    }

    @GetMapping(value = "/change-password")
    public ResponseEntity<?> getData(@PathVariable("old") String oldPass, @PathVariable("new") String newPass) {
        userService.changePassword(oldPass, newPass);

        return ResponseEntity.ok("Password changed");
    }
}
