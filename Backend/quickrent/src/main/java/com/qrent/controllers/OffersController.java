package com.qrent.controllers;

import com.qrent.dao.entities.Offer;
import com.qrent.services.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/all/offers")
public class OffersController {
    @Autowired
    private OfferService offerService;

    @PostMapping("/get")
    public List<Offer> getOffers(@RequestBody FilterDto filterDto) {
        return offerService.getOffers(filterDto.getFilters(), filterDto.getPage(), filterDto.getCount());
    }

    @PostMapping("/count")
    public Integer getCount(@RequestBody Map<String, Object> filters) {
        return offerService.count(filters);
    }

    @PostMapping(value = "/detail")
    public Offer getData(@RequestBody Map<String, Long> id) {
        return offerService.getById(id.get("id"));
    }

    @PostMapping(value = "/get-marks")
    public List<MarkDto> getMarks() {
        return offerService.getMarks();
    }
}
