package com.qrent.controllers;

import com.qrent.dao.entities.User;
import com.qrent.dto.request.LoginRequest;
import com.qrent.dto.request.SignupRequest;
import com.qrent.services.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private UserServiceImpl userService;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        return ResponseEntity.ok(userService.auth(loginRequest));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        return userService.register(signUpRequest);
    }

    @GetMapping(value = "/update-tokens/{token}")
    public ResponseEntity<?> updateTokens(@PathVariable(value = "token", required = true) String token) {
        Object response = userService.updateTokens(token);
        if (response != null) {
            return ResponseEntity.ok(response);
        } else {
            return ResponseEntity.badRequest().body("Fail");
        }
    }
}
