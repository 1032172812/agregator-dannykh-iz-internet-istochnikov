package com.qrent.controllers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
public class FilterDto {
    private Map<String, Object> filters;
    private int page;
    private int count;
}
