package com.qrent.parsers;

public enum YaroslavlDistrict implements District {
    KIROVSKIY("Кировский"),
    LENINIST("Ленинский"),
    KRASNOPEREKOPSKY("Красноперекопский"),
    ZAVOLZHSKY("Заволжский"),
    FRUNZENSKY("Фрунзенский"),
    DZERZHINSKY("Дзержинский");

    private final String name;

    YaroslavlDistrict(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
