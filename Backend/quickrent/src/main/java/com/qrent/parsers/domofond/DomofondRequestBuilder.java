package com.qrent.parsers.domofond;

import com.google.gson.Gson;
import com.qrent.parsers.RentalTime;
import com.qrent.parsers.RequestBuilder;
import com.qrent.parsers.RoomsType;
import lombok.val;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//@Component
public class DomofondRequestBuilder implements RequestBuilder {
    //    @Value("${qrent.app.maxPrice}")
//    private long MAX_PRICE;
    public static long MAX_PRICE = 100000000;
    public static int MAX_LIMIT = 100; // TODO: 200

    private Double priceFrom = 0.;
    private Double priceTo = Double.valueOf(MAX_PRICE);
    private final List<String> roomsTypes = new ArrayList<>();
    private final List<JSONObject> districtsTypes = new ArrayList<>();
    private String rentalTime = null;
    private int offset = 0;

    @Override
    public RequestBuilder offset(int offset) {
        this.offset = offset;
        return this;
    }

    @Override
    public RequestBuilder priceFrom(Double price) {
        this.priceFrom = (price == null) ? 0 : price;
        return this;
    }

    @Override
    public RequestBuilder priceTo(Double price) {
        this.priceTo = (price == null) ? MAX_PRICE : price;
        return this;
    }

    @Override
    public RequestBuilder rentalTime(RentalTime rentalTime) {
        if (rentalTime != null) {
            this.rentalTime = rentalTime.getTime();
        }

        return this;
    }

    private void addRoomsType(RoomsType roomsType) {
        if (roomsType != null) {
            roomsTypes.add(roomsType.getName());
        }
    }

    @Override
    public RequestBuilder addRoomsTypes(List<RoomsType> roomsTypes) {
        if (roomsTypes != null) {
            roomsTypes.forEach(this::addRoomsType);
        }
        return this;
    }

    private void addDistrictsType(String districtType) {
        JSONObject district;
        DomofondDistrict currentDistrict = DomofondDistrict.valueOf(districtType);

        district = new JSONObject();
        district.put("areaType", "District");
        district.put("hasDistricts", currentDistrict.hasDistricts());
        district.put("hasMetros", currentDistrict.hasMetros());
        district.put("hasRoads", currentDistrict.hasRoads());
        district.put("parentID", currentDistrict.getParentId());
        district.put("id", currentDistrict.getId());
        district.put("parentName", currentDistrict.getParentName());
        district.put("name", currentDistrict.getName());

        districtsTypes.add(district);
    }

    @Override
    public RequestBuilder addDistrictsTypes(List<String> districtsTypes) {
        if (districtsTypes != null) {
            districtsTypes.forEach(this::addDistrictsType);
        }
        return this;
    }

    private JSONArray buildLocations() {
        JSONObject coords = new JSONObject();
        coords.put("latitude", 57.622434);
        coords.put("longitude", 39.887894);

        JSONObject yarLocation = new JSONObject();
        yarLocation.put("id", 7);
        yarLocation.put("name", "Ярославская область");
        yarLocation.put("prepositionalName", "Ярославской области");
        yarLocation.put("areaType", "Region");
        yarLocation.put("hasMetros", Boolean.FALSE);
        yarLocation.put("hasDistricts", Boolean.FALSE);
        yarLocation.put("hasRoads", Boolean.FALSE);
        yarLocation.put("location", coords);

        JSONArray locationsArray = new JSONArray();
        locationsArray.add(yarLocation);

        districtsTypes.forEach(locationsArray::add);

        return locationsArray;
    }

    @Override
    public HttpUriRequest build() {
        val requestMap = new HashMap<>();
        requestMap.put("id", "1");
        requestMap.put("jsonrpc", "2.0");
        requestMap.put("method", "Item.SearchItemsV3");

        Map<String, Object> params = new HashMap<>();

        HashMap<String, Object> meta = new HashMap<>();
        meta.put("platform", "web");
        meta.put("language", "ru");

        JSONArray locations = buildLocations();

        JSONArray itemSoldByType = new JSONArray();
        itemSoldByType.add("PrivatePerson");

        HashMap<String, Object> filters = new HashMap<>();
        filters.put("itemType", "Rent");
//        filters.put("propertyType", "Apartment");
        filters.put("priceFrom", priceFrom);
        filters.put("priceTo", priceTo);
        filters.put("rooms", roomsTypes);
        filters.put("locations", locations);
        filters.put("rentalRate", rentalTime);
//        filters.put("itemSoldByType", itemSoldByType);
//        filters.put("withCommission", Boolean.FALSE);
        filters.put("withPhotos", Boolean.TRUE);
//        filters.put("publicationTimeRange", "OneWeek");

        params.put("meta", meta);
        params.put("filters", filters);
        params.put("order", "Default");
        params.put("offset", offset);
        params.put("limit", MAX_LIMIT);

        requestMap.put("params", params);

        String json = new Gson().toJson(requestMap);

        HttpPost request = new HttpPost("https://api.domofond.ru/rpc");
        request.addHeader("origin", "https://www.domofond.ru");
        request.addHeader("content-type", "text/plain");

        try {
            request.setEntity(new StringEntity(json));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return request;
    }
}
