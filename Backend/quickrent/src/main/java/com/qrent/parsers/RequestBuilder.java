package com.qrent.parsers;

import org.apache.http.client.methods.HttpUriRequest;

import java.util.List;

public interface RequestBuilder {

    /**
     * Установить смещение начала выборки офферов
     *
     * @param offset смещение
     */
    RequestBuilder offset(int offset);

    /**
     * Установить минимальную стоимость
     *
     * @param price цена
     */
    RequestBuilder priceFrom(Double price);

    /**
     * Установить максимальную стоимость
     *
     * @param price цена
     */
    RequestBuilder priceTo(Double price);

    /**
     * Установить срок аренды
     *
     * @param rentalTime срок
     */
    RequestBuilder rentalTime(RentalTime rentalTime);

    /**
     * Добавить количество комнат
     *
     * @param roomsTypes тип
     */
    RequestBuilder addRoomsTypes(List<RoomsType> roomsTypes);

    /**
     * Добавить районы
     *
     * @param districtsTypes районы
     */
    RequestBuilder addDistrictsTypes(List<String> districtsTypes);

    /**
     * Построить запрос
     *
     * @return запрос
     */
    HttpUriRequest build();
}
