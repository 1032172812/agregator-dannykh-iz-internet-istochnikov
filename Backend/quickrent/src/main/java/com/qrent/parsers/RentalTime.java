package com.qrent.parsers;

public enum RentalTime {
    DAY("Day", "Day"),
    MONTH("Month", "Month");

    private String name;
    private String time;

    RentalTime(String name, String time) {
        this.name = name;
        this.time = time;
    }

    public String getTime() {
        return time;
    }


    public String getName() {
        return name;
    }
}
