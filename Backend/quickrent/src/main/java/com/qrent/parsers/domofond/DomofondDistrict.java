package com.qrent.parsers.domofond;

import com.qrent.parsers.District;

public enum DomofondDistrict implements District {
    KIROVSKIY("Кировский", 371),
    LENINIST("Ленинский", 373),
    KRASNOPEREKOPSKY(197, 372, "Ярославль", "Красноперекопский"),
    ZAVOLZHSKY(197, 370, "Ярославль", "Заволжский"),
    FRUNZENSKY(197, 374, "Ярославль", "Фрунзенский"),
    DZERZHINSKY(197, 369, "Ярославль", "Дзержинский"),;

    private final Boolean hasDistricts;
    private final Boolean hasMetros;
    private final Boolean hasRoads;
    private final Integer parentId;
    private final Integer id;
    private final String parentName;
    private final String name;

    DomofondDistrict(String name, Integer id){
        this.name = name;
        this.id = id;

        this.hasDistricts = Boolean.FALSE;
        this.hasMetros = Boolean.FALSE;
        this.hasRoads = Boolean.FALSE;
        this.parentId = null;
        this.parentName = null;
    }

    DomofondDistrict(Integer parentId, Integer id, String parentName, String name){
        this.parentId = parentId;
        this.id = id;
        this.parentName = parentName;
        this.name = name;

        this.hasDistricts = Boolean.FALSE;
        this.hasMetros = Boolean.FALSE;
        this.hasRoads = null;
    }

    public String getName(){
        return name;
    }

    public Integer getId() {
        return id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public Boolean hasDistricts() {
        return hasDistricts;
    }

    public Boolean hasRoads() {
        return hasRoads;
    }

    public String getParentName() {
        return parentName;
    }

    public Boolean hasMetros() {
        return hasMetros;
    }
}
