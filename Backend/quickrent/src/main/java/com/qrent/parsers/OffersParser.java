package com.qrent.parsers;

import com.qrent.dao.entities.Offer;

import java.util.List;

public interface OffersParser {
    /**
     * Парсит ресурс с офферами
     *
     * @return
     */
    List<Offer> parseAll();
}
