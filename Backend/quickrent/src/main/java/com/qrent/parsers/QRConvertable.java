package com.qrent.parsers;

import com.qrent.dao.entities.Offer;

public interface QRConvertable {

    /**
     * Преобразует к стандартному офферу
     *
     * @return
     */
    Offer toStandard();
}
