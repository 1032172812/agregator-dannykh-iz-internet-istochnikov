package com.qrent.parsers.domofond;

import com.qrent.dao.entities.Location;
import com.qrent.dao.entities.Offer;
import com.qrent.parsers.OffersParser;
import com.qrent.parsers.RentalTime;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Component;

import javax.xml.bind.DatatypeConverter;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.qrent.parsers.domofond.DomofondRequestBuilder.MAX_LIMIT;

@Component
public class DomofondParser implements OffersParser {
    private static final Pattern notNum = Pattern.compile("\\D");

    @Override
    public List<Offer> parseAll() {
        Set<Offer> offers = new HashSet<>();
        HttpUriRequest request;
        Long nextOffset = null;
        int i = -1;

        do {
            i++;
            request = new DomofondRequestBuilder().offset(i * MAX_LIMIT).build();

            try (CloseableHttpClient httpClient = HttpClients.createDefault();
                 CloseableHttpResponse response = httpClient.execute(request)) {
                String responseJson = EntityUtils.toString(response.getEntity());

                JSONObject obj = (JSONObject) (new JSONParser().parse(responseJson));
                JSONObject result = (JSONObject) obj.get("result");
                nextOffset = (Long) result.get("nextOffset");


                JSONArray items = (JSONArray) result.get("items");

                if (items != null) {
                    items.forEach(item -> {
                        if (item != null) {
                            offers.add(parseOffer((JSONObject) item));
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (nextOffset != null);

        List<String> exceptWords = Arrays.asList("магазин",
                "коммерческая недвижимость",
                "офис",
                "земли",
                "островок",
                "торговый",
                "производство",
                "склад",
                "гараж");

        return offers.stream().filter(item -> {
            Boolean flag = true;
            for (String exceptWord : exceptWords) {
                if (item.getTitle().toLowerCase().contains(exceptWord)) {
                    flag = false;
                }
            }
            return flag;
        }).collect(Collectors.toList());
    }

    private String getDistrictById(Long id) {
        String name = "Неопознанный";

        for (DomofondDistrict district : DomofondDistrict.values()) {
            if (district.getId().equals(id))
                name = district.getName();
        }

        return name;
    }

    private Offer parseOffer(JSONObject json) {
        List<String> imgUrls = (List<String>) ((JSONArray) json.get("thumbnailUrls")).stream().map(item -> ((JSONObject) item).get("url").toString()).collect(Collectors.toList());
        JSONObject dataSummary = (JSONObject) json.get("dataSummary");
        String title = (String) dataSummary.get("title");
        String description = (String) dataSummary.get("description");
        double price = ((Long) dataSummary.get("priceValue")).doubleValue();

        String publishDateString = (String) dataSummary.get("publishDate");
        Calendar publishDate = null;
        if (publishDateString != null) {
            publishDate = DatatypeConverter.parseDateTime(publishDateString);
        }

        String depositString = ((String) dataSummary.get("deposit")).replaceAll(notNum.pattern(), "");
        double deposit = Double.parseDouble((depositString.equals("")) ? "0" : depositString);

        String district = getDistrictById((Long) dataSummary.get("districtId"));

        String address = (String) dataSummary.get("address");

        String floorStr = (String) dataSummary.get("floor");
        Integer floor = -1;
        Integer maxFloor = -1;

        if (floorStr != null) {
            String[] numbers = floorStr.split("/");

            if (numbers != null && numbers.length > 0) {
                try {
                    floor = Integer.parseInt(numbers[0]);
                } catch (Exception e) {
                }

                try {
                    maxFloor = Integer.parseInt(numbers[1]);
                } catch (Exception e) {
                }
            }
        }

        double area = -1.;
        if (dataSummary.get("floorAreaCalculated") != null) {
            if (dataSummary.get("floorAreaCalculated") instanceof Double) {
                area = (double) dataSummary.get("floorAreaCalculated");
            } else {
                area = ((Long) dataSummary.get("floorAreaCalculated")).doubleValue();
            }
        }

        Integer rooms = (dataSummary.get("roomsOrdinal") != null) ? ((Long) dataSummary.get("roomsOrdinal")).intValue() : null;
        String pageUrl = "https://www.domofond.ru" + dataSummary.get("itemUrl");

        JSONObject locationJson = (JSONObject) dataSummary.get("location");
        double latitude = (double) locationJson.get("latitude");
        double longitude = (double) locationJson.get("longitude");
        Location location = new Location(latitude, longitude);

        RentalTime rentalTime = (price < 5000) ? RentalTime.DAY : RentalTime.MONTH;

        return new Offer.Builder()
                .imgUrls(imgUrls)
                .title(title)
                .description(description)
                .price(price)
                .deposit(deposit)
                .district(district)
                .address(address)
                .floor(floor)
                .maxFloor(maxFloor)
                .area(area)
                .rooms(rooms)
                .pageUrl(pageUrl)
                .location(location)
                .rentalTime(rentalTime)
                .publishDate(publishDate)
                .build();
    }
}
