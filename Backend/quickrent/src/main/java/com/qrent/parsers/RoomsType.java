package com.qrent.parsers;

public enum RoomsType {
    STUDIO("Studio", -1),
    ONE("One", 1),
    TWO("Two", 2),
    THREE("Three", 3),
    FOUR_PLUS("FourPlus", 4);

    private final String name;
    private final int number;

    RoomsType(String name, int number) {
        this.name = name;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public int getNumber() {
        return number;
    }
}
