import service from '@/services/marks.service'

const value = null;

export const mark = {
  namespaced: true,
  state: {
    main: value
  },
  mutations: {
    main: (state, data) => {
      state.main = data
    }
  },
  actions: {
    main(context, props) {
      service.mark(props.id).then(
        response => {
          context.commit('main', response);
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },
  }
}