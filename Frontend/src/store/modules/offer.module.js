import service from '@/services/offers.service'

export const offer = {
  namespaced: true,
  state: {
    main: null
  },
  mutations: {
    main: (state, value) => {
      state.main = value;
    }
  },
  actions: {
    main(context, props) {
      service.offer(props.id).then(
        response => {
          context.commit('main', response);
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },
  }
}