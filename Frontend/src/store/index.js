import Vue from 'vue'
import Vuex from 'vuex'
import { offers } from './modules/offers.module'
import { offer } from './modules/offer.module'
import { mark } from './modules/mark.module'
import { marks } from './modules/marks.module'
import { pagination } from './modules/pagination.module'
import { filters } from './modules/filters.module'
import { smart } from './modules/smart.module'
import { auth } from './modules/auth.module'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    offer,
    offers,
    mark,
    marks,
    filters,
    smart,
    pagination,
    auth
  }
})

export default store