import axios from "axios";

const api = "/api/all/offers/";

class OfferService {
  offer(id) {
    return axios
    .post(api + "detail/", {
      id: id
    })
    .then(response => {
      return response.data;
    });
  }

  list(num, page, filters) {
    return axios
      .post(api + "get/", {
        filters: filters,
        count: num,
        page: page
      })
      .then(response => {
        return response.data;
      });
  }
}

export default new OfferService();