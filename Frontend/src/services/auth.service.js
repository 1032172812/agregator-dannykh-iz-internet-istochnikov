import axios from "axios";

const api = "/api/auth/";

class AuthService {
  signin(user) {
    return axios
    .post(api + "signin/", {
      username: user.username,
      password: user.password
    })
    .then(response => {
      if (response.data.accessToken && response.data.refreshToken) {
        localStorage.setItem("user", JSON.stringify(response.data));
      }
      return response.data;
    });
  }

  signup(user) {
    return axios.post(api + "signup/", {
      username: user.username,
      email: user.email,
      password: user.password,
    });
  }

  refresh(user) {
    return axios.get(api + "update-tokens/" + JSON.parse(user).refreshToken)
    .then(response => {
      if (response.data.accessToken && response.data.refreshToken) {
        localStorage.setItem("user", JSON.stringify(response.data));
      }
      return response.data;
    });
  }

  logout() {
    localStorage.removeItem("user");
  }
}

export default new AuthService();