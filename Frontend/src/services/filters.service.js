import axios from "axios";

class FiltersService {
  get() {
    var data = require('@/data/modules/filters');
    var result = JSON.parse(JSON.stringify(data));
    return result;
  }
  
  grouping(type) {
    var data = require('@/data/modules/filters')
    var result = JSON.parse(JSON.stringify(data))

    switch (type) {
      case 'MONTH':
        result['rent-type']['value'] = 'MONTH'
        break;
      case 'DAY':
        result['rent-type']['value'] = 'DAY'
        break;
      default:
        break;
    }

    return result
  }
}

export default new FiltersService();