import axios from "axios";
import token from '@/services/token.service'

const user = "user/";

class UserService {
  getData() {
    console.log("Вызываю ошибку.")
    axios.get(user + "get-data/", { headers: token.getToken });
  }
}

export default new UserService();