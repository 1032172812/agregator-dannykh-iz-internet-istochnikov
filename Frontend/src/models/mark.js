export default class Mark {
  constructor(id, lat, long) {
    this.id = id;
    this.lat = lat;
    this.long = long;
  }
}