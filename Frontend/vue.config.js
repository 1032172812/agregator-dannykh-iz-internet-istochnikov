module.exports = {
  pwa: {
    name: 'Квик',
    themeColor: '#00af73',
    msTileColor: '#00af73'
  },
  devServer: {
    proxy: 'http://localhost:8080'
  }
}
